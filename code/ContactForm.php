<?php
// Helper class for creating contact forms with Silverstripe.
class ContactForm extends Form
{
    public function __construct($controller, $formname, $fields=null)
    {
        // Create a default fieldlist.
        if(is_null($fields))
        {
            $fields = new FieldList(
            TextField::create("Name", "")->setAttribute("placeholder", "Name"),
            TextField::create("Telephone", "")->setAttribute("placeholder", "Telephone"),
            TextField::create("Email", "")->setAttribute("placeholder", "Email"),
            TextAreaField::create("Message", "")->setAttribute("placeholder", "Message")
          );
        }

        // Set the form action and use the parent class to create the form.
        $actions = new FieldList(new FormAction('submitForm', 'Submit'));
        parent::__construct($controller, $formname, $fields, $actions);
    }

    // Action to submit the contact form
    // $data - data from the form submission
    // $form - the Silverstripe form object.
    public function submitForm($data, $form)
    {
        // Load the site config and field data.
        $config = SiteConfig::current_site_config();
        $fields = $form->getData();

        // Start setting up the content for an email notification.
        $subject = "New form submission from the website.";
        $message = "<p>";

        // Loop through the data, adding each item to the email content.
        foreach($fields as $name => $value)
        {
            $message .= $name.": ".$value;
            if(end($fields) != $value) {$message .= "<br>";}
        }

        $message .= "</p>";

        // Create and send the email
        $email = new Email($config->FormEmail, $config->FormEmail, $subject, $message);
        $email->send();

        // Redirect the user to a thank you page.
        Controller::redirect("/thanks");
    }

    // Add a new field to the contact form
    public function addField($field, $before=null)
    {
        $fields = $this->Fields();

        if($before != null)
        {
            $fields->insertBefore($field, $before);
        }
        else
        {
            $fields->push($field);
        }
        $this->setFields($fields);
    }

    // Add multiple fields to the contact form
    public function addFields($fields, $before=null)
    {
        $fields = $this->Fields();

        foreach($fields as $field)
        {
            $this->addField($field, $before);
        }

        $this->setFields($fields);
    }

    // Remove a field from the contact form
    public function removeField($field)
    {
        $fields = $this->Fields();
        $fields->removeByName($field);
        $this->setFields($fields);
    }
}
