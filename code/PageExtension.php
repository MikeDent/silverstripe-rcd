<?php
// Data Extension class to add MetaTitle Field to Silverstripe pages.
class PageExtension extends DataExtension
{
    // Add MetaTitle field to the database table
    private static $db = array("MetaTitle"=>"Text");

		// Add the MetaTitle field to the CMS admin page.
		// $fields - exisiting admin form.				 
    public function updateCMSFields(FieldList $fields)
		{
		$metaFieldTitle = new TextField("MetaTitle", $this->owner->fieldLabel('MetaTitle'));
		$metaFieldTitle->setRightTitle('Shown at the top of the browser window and used as the "linked text" by search engines.')->addExtraClass('help');
		$fields->insertBefore($metaFieldTitle, Config::inst()->get('PageExtension', 'InsertBefore'));
		return $fields;
	  }

}
