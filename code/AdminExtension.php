<?php
class AdminExtension extends LeftAndMainExtension
{
    // Remove unwanted options from the admin menu to keep things simple.
    public function init()
    {
        CMSMenu::remove_menu_item('Help');
        CMSMenu::remove_menu_item('ReportAdmin');
    }
}
