<?php
// Adds a SiteMap page type to Silverstripe and creates a Sitemap and Thank You Page when a new site is built.
class SiteMap extends Page
{
    // Hide from the main navigation menu.
    private static $defaults = array("ShowInMenus"=>false);

    // Extend the Dev/Build task to create the new pages in the CMS.
    public function requireDefaultRecords()
    {
        parent::requireDefaultRecords();

        // Create a sitemap page and publish it.
		    if(class_exists('SiteMap') && !DataObject::get_one('SiteMap'))
				{
				    $sitemap = new SiteMap();
						$sitemap->Title = "Site Map";
						$sitemap->Content = "";
						$sitemap->URLSegment = "sitemap";
						$sitemap->write();
						$sitemap->publish('Stage', 'Live');
						$sitemap->flushCache();
						DB::alteration_message('Sitemap page created', 'created');

            // Create a thank you page for the contact form, and publish it.
            $thanks = new Page();
						$thanks->Title = "Thank You";
						$thanks->Content = "Thank you for contacting us, we will be in touch shortly.";
						$thanks->URLSegment = "thanks";
						$thanks->ShowInMenus = false;
						$thanks->write();
						$thanks->publish('Stage', 'Live');
						$thanks->flushCache();
						DB::alteration_message('Thank You page created', 'created');
        }

    }
}

class SiteMap_Controller extends Page_Controller
{
    // Get all pages for the sitemap, regardless of whether they appear in the navigation menu.
    function Pages()
    {
        return DataObject::get("Page", "ParentID = 0");
    }

}
?>
