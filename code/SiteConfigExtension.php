<?php
// Data extension to allow company contact details to be added via a settings page, to be ued throughout a Silverstripe website.
class SiteConfigExtension extends DataExtension
{
    // Add fields to the database
    private static $db = array(
    'Address' => 'Text',
		'Telephone' => 'Text',
		'Email' => 'Text',
		'Facebook' => 'Text',
		'Twitter' => 'Text',
    'FormEmail' => 'Text'
    );

    // Add fields to the CMS admin page
    // $fields - exisiting admin form.
    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab("Root.Main",
        array(
        TextField::create('Email'),
        TextField::create('Telephone'),
        TextField::create('Facebook'),
        TextField::create('Twitter'),
        TextField::create('FormEmail', 'Email Adress to send form submissions'),
        TextField::create('Address', 'Address (comma seperated)')
        )
        );

        // Remove tagline field
        $fields->removeFieldFromTab("Root.Main", "Tagline");
    }
}
