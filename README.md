# Silverstripe/RCD

A Silverstripe starter for Red Chilli websites. Includes:

- A contact form helper class
- A Sitemap page type and generator
- Customisations to the CMS admin page
